import { ApiResponseModel } from "../models/ApiResponseModel";
import Cookies from "js-cookie";

const ENDPOINT = process.env.VUE_APP_SERVER_URL;
const LOCALSTORAGE_REFRESH_TOKEN = "refresh_token";
const ACCESS_TOKEN = "access_token";

let refreshRequestSent = false;

/**
 *
 * @param {Object} userCredentials
 * @returns {Promise<ApiResponseModel>} Server response with success status (true/false) and an optional error message
 * @see {@link ApiResponseModel}
 */
export async function register(userCredentials) {
  const url = ENDPOINT + "/api/register";

  try {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(userCredentials),
    });

    if (response.ok) {
      return ApiResponseModel.success(null);
    } else if (response.status === 400) {
      return ApiResponseModel.fail(
        "User registration failed! Username or email is already taken!"
      );
    } else {
      return ApiResponseModel.fail(
        `Registration request failed. Status code: ${response.status}`
      );
    }
  } catch (error) {
    return ApiResponseModel.fail(error);
  }
}

/**
 *
 * @param {Object} userCredentials
 * @returns {Promise<ApiResponseModel>} Server response with success status (true/false) and JWT + additional data on
 * success or error message on failure
 * @see {@link ApiResponseModel}
 */
export async function signIn(userCredentials) {
  const url = ENDPOINT + "/api/auth";

  try {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(userCredentials),
    });

    if (response.ok) {
      const responseData = await response.json();
      localStorage.setItem(
        LOCALSTORAGE_REFRESH_TOKEN,
        responseData.refreshToken
      );
      Cookies.set(ACCESS_TOKEN, responseData.accessToken);

      return ApiResponseModel.success(responseData);
    } else if (response.status === 400) {
      return ApiResponseModel.fail("Invalid email or password!");
    } else {
      return ApiResponseModel.fail(
        `Login request failed. Status code: ${response.status}`
      );
    }
  } catch (error) {
    return ApiResponseModel.fail(error);
  }
}

/**
 *
 * @returns {Promise<ApiResponseModel>} Server response with success status (true/false) and an optional error message
 * @see {@link ApiResponseModel}
 */
export async function renewToken() {
  const refreshToken = localStorage.getItem(LOCALSTORAGE_REFRESH_TOKEN);
  if (!refreshToken) {
    return ApiResponseModel.fail("No refresh token!");
  }

  const tokenParam = "refresh-token=" + refreshToken;
  const url = ENDPOINT + "/api/auth/refresh?" + tokenParam;

  try {
    const response = await fetch(url);
    refreshRequestSent = true;

    if (response.ok) {
      const responseData = await response.json();
      localStorage.setItem(
        LOCALSTORAGE_REFRESH_TOKEN,
        responseData.refreshToken
      );
      Cookies.set(ACCESS_TOKEN, responseData.accessToken);
      return ApiResponseModel.success(responseData);
    } else if (response.status === 400) {
      return ApiResponseModel.fail("Invalid refresh token");
    } else {
      return ApiResponseModel.fail(
        `JWT refresh request failed. Status code: ${response.status}`
      );
    }
  } catch (error) {
    return ApiResponseModel.fail(error);
  } finally {
    refreshRequestSent = false;
  }
}

/**
 * Removes the saved refresh token and sends a request to revoke the token on the server side
 */
export function logout() {
  Cookies.remove(ACCESS_TOKEN);
  const refreshToken = localStorage.getItem(LOCALSTORAGE_REFRESH_TOKEN);
  const tokenParam = "refresh-token=" + refreshToken;
  const url = ENDPOINT + "/api/logout?" + tokenParam;
  fetch(url);
  localStorage.removeItem(LOCALSTORAGE_REFRESH_TOKEN);
}

/**
 * @returns {string|null} Access token
 */
export function getJwtFromCookies() {
  const token = Cookies.get(ACCESS_TOKEN);
  return token ? token : null;
}
