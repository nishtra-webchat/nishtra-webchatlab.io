//import Vuex from "vuex";
import jwt_decode from "jwt-decode";
import { renewToken, logout as authServiceLogout } from "../services/auth";
import router from "../router";
import * as Helpers from "./../utils/helpers.js";

const auth = {
  state: {
    accessToken: null,
    refreshTokenTimeoutId: null,
  },
  getters: {
    username: (state) => {
      if (state.accessToken === null) return null;

      try {
        const jwtPayload = jwt_decode(state.accessToken);
        const username = jwtPayload.username;
        return username;
      } catch (error) {
        return null;
      }
    },
    userid: (state) => {
      if (state.accessToken === null) return null;

      try {
        const jwtPayload = jwt_decode(state.accessToken);
        const userid = jwtPayload.user_id;
        return userid;
      } catch (error) {
        return null;
      }
    },
    isAuthenticated: (state) => {
      if (state.accessToken === null) return false;

      try {
        const jwtPayload = jwt_decode(state.accessToken);
        const expTimestamp = jwtPayload.exp * 1000;
        return expTimestamp > Date.now();
      } catch (error) {
        return false;
      }
    },
  },
  mutations: {
    setAccessToken(state, value) {
      state.accessToken = value;
    },
    setRefreshTokenTimeoutId(state, value) {
      state.refreshTokenTimeoutId = value;
    },
  },
  actions: {
    resetAuthData({ commit }) {
      commit("setAccessToken", null);
    },
    setRefreshTokenTimer({ commit, state }) {
      if (state.accessToken === null) return;
      if (state.refreshTokenTimeoutId !== null) {
        clearTimeout(state.refreshTokenTimeoutId);
      }

      const refreshTimeout = getJwtRefreshTimeout(state.accessToken);
      const timeoutId = setTimeout(
        (commit, state) => {
          refreshAccessToken(commit, state);
        },
        refreshTimeout,
        commit,
        state
      );
      commit("setRefreshTokenTimeoutId", timeoutId);
    },
    logout({ commit, state }) {
      if (state.refreshTokenTimeoutId !== null) {
        clearTimeout(state.refreshTokenTimeoutId);
      }
      commit("setRefreshTokenTimeoutId", null);
      commit("setAccessToken", null);
      router.replace("/");
      authServiceLogout();
    },
  },
};

export default auth;

async function refreshAccessToken(commit, state) {
  try {
    const response = await renewToken();
    if (response.requestSuccess) {
      commit("setAccessToken", response.payload.accessToken);

      const refreshTimeout = getJwtRefreshTimeout(response.payload.accessToken);
      Helpers.devlog(
        "Next token refresh",
        new Date(Date.now() + refreshTimeout)
      );
      const timeoutId = setTimeout(
        refreshAccessToken,
        refreshTimeout,
        commit,
        state
      );
      commit("setRefreshTokenTimeoutId", timeoutId);
    } else {
      console.log("Error during refresh token request");
    }
  } catch (error) {
    console.log(error);
  }
}

function getJwtRefreshTimeout(jwt) {
  const parsedJwt = jwt_decode(jwt);
  const expTimestamp = parsedJwt.exp * 1000;
  const refreshTimeout = expTimestamp - Date.now() - 60 * 1000;
  return refreshTimeout;
}
