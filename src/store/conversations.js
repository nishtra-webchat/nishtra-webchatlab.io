import * as Helpers from "../utils/helpers.js";
import { getUserConversations } from "../services/chatApi.js";

const conversations = {
  state: {
    conversations: [],
    loadingRequestsProcessed: 0,
  },
  getters: {
    conversations: (state) => state.conversations,
    isLoadingConversations: (state) => state.loadingRequestsProcessed > 0,
  },
  mutations: {
    addConversation(state, conversation) {
      state.conversations.push(conversation);
    },
    clearConversations(state) {
      state.conversations = [];
    },
    setConversationCollection(state, conversations) {
      state.conversations = conversations;
    },
    sortConversations(state) {
      state.conversations.sort(Helpers.compareConversations);
    },
    addUnreadMessage(state, message) {
      let conversationId = message.conversationId;
      state.conversations
        .filter((x) => x.id == conversationId)
        .forEach((x) => {
          x.lastMessage = message;
          x.newMessages += 1;
        });
    },
    clearUnreadMessages(state, conversationId) {
      state.conversations
        .filter((x) => x.id == conversationId)
        .forEach((x) => (x.newMessages = 0));
    },
    setLoadingState(state, isLoading) {
      if (isLoading) {
        state.loadingRequestsProcessed += 1;
      } else {
        state.loadingRequestsProcessed -= 1;
      }
    },
  },
  actions: {
    addConversation({ commit, state }, conversation) {
      commit("addConversation", conversation);
      commit("sortConversations");
    },
    async loadConversations({ commit, state }) {
      commit("setLoadingState", true);
      let response = await getUserConversations();
      if (response.requestSuccess) {
        commit("setConversationCollection", response.payload);
      } else {
        commit("clearConversations");
        console.log(response.error);
      }
      commit("setLoadingState", false);
    },
  },
};

export default conversations;
