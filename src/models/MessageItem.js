export class MessageItem {
  conversationId = -1;
  id = -1;
  senderId = -1;
  senderName = null;
  sentAt = null;
  text = null;
  isDelivered = undefined;

  static createEmpty() {
    return new MessageItem();
  }

  static create(conversationId, senderId, senderName, text) {
    let msg = new MessageItem();
    msg.id = Math.floor(-Math.random() * 1000);
    msg.conversationId = conversationId;
    msg.senderId = senderId;
    msg.senderName = senderName;
    msg.sentAt = new Date().toISOString();
    msg.text = text;
    msg.isDelivered = false;
    return msg;
  }
}
